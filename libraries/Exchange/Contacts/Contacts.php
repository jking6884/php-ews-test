<?php
namespace Libraries\Exchange\Contacts;

use \jamesiarmes\PhpEws\Client;
use \jamesiarmes\PhpEws\Request\GetItemType;
use \jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfBaseItemIdsType;
use \jamesiarmes\PhpEws\Enumeration\DefaultShapeNamesType;
use \jamesiarmes\PhpEws\Enumeration\ResponseClassType;
use \jamesiarmes\PhpEws\Type\ItemIdType;
use \jamesiarmes\PhpEws\Type\ItemResponseShapeType;
use \jamesiarmes\PhpEws\Request\FindItemType;
use \jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfBaseFolderIdsType;
use \jamesiarmes\PhpEws\Enumeration\DistinguishedFolderIdNameType;
use \jamesiarmes\PhpEws\Type\ContactsViewType;
use \jamesiarmes\PhpEws\Type\DistinguishedFolderIdType;
use \jamesiarmes\PhpEws\Request\CreateItemType;
use \jamesiarmes\PhpEws\Enumeration\BodyTypeType;
use \jamesiarmes\PhpEws\Enumeration\EmailAddressKeyType;
use \jamesiarmes\PhpEws\Enumeration\FileAsMappingType;
use \jamesiarmes\PhpEws\Enumeration\MapiPropertyTypeType;
use \jamesiarmes\PhpEws\Enumeration\PhoneNumberKeyType;
use \jamesiarmes\PhpEws\Enumeration\PhysicalAddressKeyType;
use \jamesiarmes\PhpEws\Type\BodyType;
use \jamesiarmes\PhpEws\Type\ContactItemType;
use \jamesiarmes\PhpEws\Type\EmailAddressDictionaryEntryType;
use \jamesiarmes\PhpEws\Type\EmailAddressDictionaryType;
use \jamesiarmes\PhpEws\Type\ExtendedPropertyType;
use \jamesiarmes\PhpEws\Type\PathToExtendedFieldType;
use \jamesiarmes\PhpEws\Type\PhoneNumberDictionaryEntryType;
use \jamesiarmes\PhpEws\Type\PhysicalAddressDictionaryEntryType;
use \jamesiarmes\PhpEws\Type\PhoneNumberDictionaryType;
use \jamesiarmes\PhpEws\Request\UpdateItemType;
use \jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfItemChangeDescriptionsType;
use \jamesiarmes\PhpEws\Enumeration\ConflictResolutionType;
use \jamesiarmes\PhpEws\Enumeration\DictionaryURIType;
use \jamesiarmes\PhpEws\Type\DeleteItemFieldType;
use \jamesiarmes\PhpEws\Type\ItemChangeType;
use \jamesiarmes\PhpEws\Type\PathToIndexedFieldType;
use \jamesiarmes\PhpEws\Type\PathToUnindexedFieldType;
use \jamesiarmes\PhpEws\Type\SetItemFieldType;

class Contacts {

    private $host = '';
    private $username = '';
    private $password = '';
    private $version = '';

    private $client = null;
    private $request = null;

    private $timezone = 'Eastern Standard Time';

    private $sync_state = null;

    public function __construct($host, $username, $password, $version = Client::VERSION_2016)
    {
        $this->host = $host;
        $this->username = $username;
        $this->password = $password;
        $this->version = $version;

        $this->client = new Client($host, $username, $password, $version);
        $this->client->setTimezone($this->timezone);
    }

    public function findContacts ()
    {
        $request = new FindItemType();
        $request->ParentFolderIds = new NonEmptyArrayOfBaseFolderIdsType();
        $request->ContactsView = new ContactsViewType();
        // Find contacts in the contacts folder.
        $folder_id = new DistinguishedFolderIdType();
        $folder_id->Id = DistinguishedFolderIdNameType::CONTACTS;
        $request->ParentFolderIds->DistinguishedFolderId[] = $folder_id;
        $response = $this->client->FindItem($request);

        // Iterate over the results, printing any error messages or contact ids.
        $response_messages = $response->ResponseMessages->FindItemResponseMessage;

        foreach ($response_messages as $response_message) {
            // Make sure the request succeeded.
            if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {
                $code = $response_message->ResponseCode;
                $message = $response_message->MessageText;
                fwrite(
                    STDERR,
                    "Failed to search for contacts with \"$code: $message\"\n"
                );
                continue;
            }
            // Iterate over the contacts that were found, printing the id of each.
            return $response_message->RootFolder->Items->Contact;
        }
    }

    public function describeContacts (array $contacts_ids)
    {
        $request = new GetItemType();
        $request->ItemShape = new ItemResponseShapeType();
        $request->ItemShape->BaseShape = DefaultShapeNamesType::ALL_PROPERTIES;
        $request->ItemIds = new NonEmptyArrayOfBaseItemIdsType();

        // Iterate over the contact ids, setting each one on the request.
        foreach ($contacts_ids as $contact_id) {
            $item = new ItemIdType();
            $item->Id = $contact_id;
            $request->ItemIds->ItemId[] = $item;
        }

        $response = $this->client->GetItem($request);

        $response_messages = $response->ResponseMessages->GetItemResponseMessage;

        $successful_responses = [];

        foreach ($response_messages as $response_message) {
            // Make sure the request succeeded.
            if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {
                $code = $response_message->ResponseCode;
                $message = $response_message->MessageText;
                fwrite(STDERR, "Failed to get contact with \"$code: $message\"\n");
                continue;
            }
            // Iterate over the contacts.
            foreach ($response_message->Items->Contact as $item) {
                $successful_responses[] = $response_message;
//                $name = $item->DisplayName;
//                fwrite(STDOUT, "Retrieved contact $name\n");
            }
        }

        return $successful_responses;
    }

    public function addContact (\Libraries\Exchange\Contacts\Contact $newContact)
    {
        $request = new CreateItemType();
        $contact = new ContactItemType();

        $contact->GivenName = $newContact->givenName;
        $contact->Surname = $newContact->surname;
        $contact->PhoneNumbers = new PhoneNumberDictionaryType();
        $contact->EmailAddresses = new EmailAddressDictionaryType();
        $contact->PhoneNumbers = new PhoneNumberDictionaryType();
        $contact->ExtendedProperty = new ExtendedPropertyType();
        // Set an email address.
        $email = new EmailAddressDictionaryEntryType();
        $email->Key = EmailAddressKeyType::EMAIL_ADDRESS_1;
        $email->_ = $newContact->email;
        $contact->EmailAddresses->Entry[] = $email;
        // Set an address.
        $address = new PhysicalAddressDictionaryEntryType();
        $address->Key = PhysicalAddressKeyType::HOME;
        $address->Street = $newContact->street;
        $address->City = $newContact->city;
        $address->State = $newContact->state;
        $address->PostalCode = $newContact->postalCode;
        $address->CountryOrRegion = $newContact->countryOrRegion;
        $contact->PhysicalAddresses->Entry[] = $address;
        // Set a phone number.
        $phone = new PhoneNumberDictionaryEntryType();
        $phone->Key = PhoneNumberKeyType::HOME_PHONE;
        $phone->_ = $newContact->phoneNumber;
        $contact->PhoneNumbers->Entry[] = $phone;
        // Set contact title as an extended property.
        $property = new ExtendedPropertyType();
        $property->ExtendedFieldURI = new PathToExtendedFieldType();
        $property->ExtendedFieldURI->PropertyTag = '0x3A45';
        $property->ExtendedFieldURI->PropertyType = MapiPropertyTypeType::STRING;
        $property->Value = $newContact->title;
        $contact->ExtendedProperty = $property;
        // Set the "file as" mapping to "first name last name".
        $contact->FileAsMapping = FileAsMappingType::FIRST_SPACE_LAST;
        // Set the contact body (this is the "Notes" field in Outlook).
        $contact->Body = new BodyType();
        $contact->Body->BodyType = BodyTypeType::TEXT;
        $contact->Body->_ = $newContact->body;

        $request->Items->Contact[] = $contact;
        $response = $this->client->CreateItem($request);
        // Iterate over the results, printing any error messages or contact ids.
        $response_messages = $response->ResponseMessages->CreateItemResponseMessage;

        $successful_responses = [];

        foreach ($response_messages as $response_message) {
            // Make sure the request succeeded.
            if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {
                $code = $response_message->ResponseCode;
                $message = $response_message->MessageText;
                fwrite(STDERR, "Contact failed to create with \"$code: $message\"\n");
                continue;
            } else {
                $successful_responses[] = $response_message;
            }
        }

        return $successful_responses;
    }

    public function updateContacts(array $contact_updates)
    {
        // Build the request.
        $request = new UpdateItemType();
        $request->ConflictResolution = ConflictResolutionType::ALWAYS_OVERWRITE;

        // Iterate over the contacts to be updated.
        // TODO: refactor this to set the supplied values dynamically for each contact update
        foreach ($contact_updates as $update) {
            // Build out item change request.
            $change = new ItemChangeType();
            $change->ItemId = new ItemIdType();
            $change->ItemId->Id = $update['id'];
            $change->Updates = new NonEmptyArrayOfItemChangeDescriptionsType();
            // If an email is set, then we need to set it. Otherwise, we need to delete
            // the current value.
            if (!empty($update['email'])) {
                $field = new SetItemFieldType();
                $field->IndexedFieldURI = new PathToIndexedFieldType();
                $field->IndexedFieldURI->FieldURI = DictionaryURIType::CONTACTS_EMAIL_ADDRESS;
                $field->IndexedFieldURI->FieldIndex = EmailAddressKeyType::EMAIL_ADDRESS_1;
                $field->Contact = new ContactItemType();
                $field->Contact->EmailAddresses = new EmailAddressDictionaryType();
                $entry = new EmailAddressDictionaryEntryType();
                $entry->_ = $update['email'];
                $entry->Key = EmailAddressKeyType::EMAIL_ADDRESS_1;
                $field->Contact->EmailAddresses->Entry = $entry;
                $change->Updates->SetItemField[] = $field;
            } else {
                $field = new DeleteItemFieldType();
                $field->IndexedFieldURI = new PathToUnindexedFieldType();
                $field->IndexedFieldURI->FieldURI = DictionaryURIType::CONTACTS_EMAIL_ADDRESS;
                $field->IndexedFieldURI->FieldIndex = EmailAddressKeyType::EMAIL_ADDRESS_1;
                $change->Updates->DeleteItemField[] = $field;
            }
            $request->ItemChanges[] = $change;
        }

        $response = $this->client->UpdateItem($request);

        $successful_responses = [];

        // Iterate over the results, printing any error messages or ids of contacts that
        // were updated.
        $response_messages = $response->ResponseMessages->UpdateItemResponseMessage;
        foreach ($response_messages as $response_message) {
            // Make sure the request succeeded.
            if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {
                $code = $response_message->ResponseCode;
                $message = $response_message->MessageText;
                fwrite(STDERR, "Failed to update contact with \"$code: $message\"\n");
                continue;
            } else {
                $successful_responses[] = $response_message;
            }
        }

        return $successful_responses;
    }
}