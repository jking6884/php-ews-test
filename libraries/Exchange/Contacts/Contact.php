<?php
namespace Libraries\Exchange\Contacts;

class Contact
{
    public $givenName = '';
    public $surname = '';
    public $email = '';
    public $street = '';
    public $city = '';
    public $state = '';
    public $postalCode = '';
    public $countryOrRegion = '';
    public $phoneNumber = '';
    public $title = '';
    public $body = '';

    public function __construct(array $options) {
        if (isset($options['givenName'])) {
            $this->givenName = $options['givenName'];
        }
        if (isset($options['surname'])) {
            $this->surname = $options['surname'];
        }
        if (isset($options['email'])) {
            $this->email = $options['email'];
        }
        if (isset($options['street'])) {
            $this->email = $options['street'];
        }
        if (isset($options['city'])) {
            $this->email = $options['city'];
        }
        if (isset($options['state'])) {
            $this->email = $options['state'];
        }
        if (isset($options['postalCode'])) {
            $this->email = $options['postalCode'];
        }
        if (isset($options['countryOrRegion'])) {
            $this->email = $options['countryOrRegion'];
        }
        if (isset($option['phoneNumber'])) {
            $this->phoneNumber = $options['phoneNumber'];
        }
        if (isset($options['title'])) {
            $this->title = $options['title'];
        }
        if (isset($options['body'])) {
            $this->body = $options['body'];
        }
    }
}