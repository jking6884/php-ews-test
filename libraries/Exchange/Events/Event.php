<?php
namespace Libraries\Exchange\Events;

class Event
{
    public $start = '';
    public $end = '';
    public $subject = '';
    public $guests = '';
    public $body = '';

    public function __construct(array $options) {
        if (isset($options['start'])) {
            $this->start = $options['start'];
        }
        if (isset($options['end'])) {
            $this->end = $options['end'];
        }
        if (isset($options['subject'])) {
            $this->subject = $options['subject'];
        }
        if (isset($options['guests'])) {
            $this->guests = $options['guests'];
        }
        if (isset($options['body'])) {
            $this->body = $options['body'];
        }
    }
}