<?php
namespace Libraries\Exchange\Events;

use \jamesiarmes\PhpEws\Client;
use \jamesiarmes\PhpEws\Enumeration\ResponseClassType;
use \jamesiarmes\PhpEws\Request\CreateItemType;
use \jamesiarmes\PhpEws\Enumeration\BodyTypeType;
use \jamesiarmes\PhpEws\Type\BodyType;
use \jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfAllItemsType;
use \jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfAttendeesType;
use \jamesiarmes\PhpEws\Enumeration\CalendarItemCreateOrDeleteOperationType;
use \jamesiarmes\PhpEws\Enumeration\RoutingType;
use \jamesiarmes\PhpEws\Type\AttendeeType;
use \jamesiarmes\PhpEws\Type\CalendarItemType;
use \jamesiarmes\PhpEws\Type\EmailAddressType;
use \jamesiarmes\PhpEws\Request\FindItemType;
use \jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfBaseFolderIdsType;
use \jamesiarmes\PhpEws\Enumeration\DefaultShapeNamesType;
use \jamesiarmes\PhpEws\Enumeration\DistinguishedFolderIdNameType;
use \jamesiarmes\PhpEws\Type\CalendarViewType;
use \jamesiarmes\PhpEws\Type\DistinguishedFolderIdType;
use \jamesiarmes\PhpEws\Type\ItemResponseShapeType;
use \jamesiarmes\PhpEws\Request\GetItemType;
use \jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfPathsToElementType;
use \jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfBaseItemIdsType;
use \jamesiarmes\PhpEws\Enumeration\DistinguishedPropertySetType;
use \jamesiarmes\PhpEws\Enumeration\MapiPropertyTypeType;
use \jamesiarmes\PhpEws\Type\PathToExtendedFieldType;
use \jamesiarmes\PhpEws\Type\ItemIdType;
use \jamesiarmes\PhpEws\Request\UpdateItemType;
use \jamesiarmes\PhpEws\ArrayType\NonEmptyArrayOfItemChangeDescriptionsType;
use \jamesiarmes\PhpEws\Enumeration\CalendarItemUpdateOperationType;
use \jamesiarmes\PhpEws\Enumeration\ConflictResolutionType;
use \jamesiarmes\PhpEws\Enumeration\UnindexedFieldURIType;
use \jamesiarmes\PhpEws\Type\ItemChangeType;
use \jamesiarmes\PhpEws\Type\PathToUnindexedFieldType;
use \jamesiarmes\PhpEws\Type\SetItemFieldType;

class Events {

    private $host = '';
    private $username = '';
    private $password = '';
    private $version = '';

    private $client = null;
    private $request = null;

    private $timezone = 'Eastern Standard Time';

    private $sync_state = null;

    public function __construct($host, $username, $password, $version = Client::VERSION_2016)
    {
        $this->host = $host;
        $this->username = $username;
        $this->password = $password;
        $this->version = $version;

        $this->client = new Client($host, $username, $password, $version);
        $this->client->setTimezone($this->timezone);
    }

    public function findEvents($options)
    {
        $this->client->setTimezone($options['timezone']);
        $request = new FindItemType();
        $request->ParentFolderIds = new NonEmptyArrayOfBaseFolderIdsType();
        // Return all event properties.
        $request->ItemShape = new ItemResponseShapeType();
        $request->ItemShape->BaseShape = DefaultShapeNamesType::ALL_PROPERTIES;
        $folder_id = new DistinguishedFolderIdType();
        $folder_id->Id = DistinguishedFolderIdNameType::CALENDAR;
        $request->ParentFolderIds->DistinguishedFolderId[] = $folder_id;
        $request->CalendarView = new CalendarViewType();
        $request->CalendarView->StartDate = $options['start']->format('c');
        $request->CalendarView->EndDate = $options['end']->format('c');
        $response = $this->client->FindItem($request);

        // Iterate over the results, printing any error messages or event ids.
        $response_messages = $response->ResponseMessages->FindItemResponseMessage;

        foreach ($response_messages as $response_message) {
            // Make sure the request succeeded.
            if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {
                $code = $response_message->ResponseCode;
                $message = $response_message->MessageText;
                fwrite(
                    STDERR,
                    "Failed to search for events with \"$code: $message\"\n"
                );
                continue;
            }

            return $response_message->RootFolder->Items->CalendarItem;
        }
    }

    public function describeEvents (array $event_ids)
    {
        // Replace with the timezone you would like the event times displayed in or
        // clear for UTC.
        $timezone = 'Eastern Standard Time';

        $this->client->setTimezone($timezone);
        // Build the request.
        $request = new GetItemType();
        $request->ItemShape = new ItemResponseShapeType();
        $request->ItemShape->BaseShape = DefaultShapeNamesType::ALL_PROPERTIES;
        $request->ItemIds = new NonEmptyArrayOfBaseItemIdsType();
        // We want to get the online meeting link in the response. Note that if this
        // property is not set on the event, it will not be included in the response.
        $property = new PathToExtendedFieldType();
        $property->PropertyName = 'OnlineMeetingExternalLink';
        $property->PropertyType = MapiPropertyTypeType::STRING;
        $property->DistinguishedPropertySetId = DistinguishedPropertySetType::PUBLIC_STRINGS;
        $additional_properties = new NonEmptyArrayOfPathsToElementType();
        $additional_properties->ExtendedFieldURI[] = $property;
        $request->ItemShape->AdditionalProperties = $additional_properties;
        // Iterate over the event ids, setting each one on the request.
        foreach ($event_ids as $event_id) {
            $item = new ItemIdType();
            $item->Id = $event_id;
            $request->ItemIds->ItemId[] = $item;
        }

        $response = $this->client->GetItem($request);

        $response_messages = $response->ResponseMessages->GetItemResponseMessage;

        $successful_responses = [];

        foreach ($response_messages as $response_message) {
            // Make sure the request succeeded.
            if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {
                $message = $response_message->ResponseCode;
                fwrite(STDERR, "Failed to get event with \"$message\"\n");
                continue;
            }
            // Iterate over the events, printing the title for each.
            foreach ($response_message->Items->CalendarItem as $item) {
                $successful_responses[] = $item;
            }
        }

        return $successful_responses;
    }

    public function addEvent (\Libraries\Exchange\Events\Event $eventData)
    {
        $request = new CreateItemType();
        $request->SendMeetingInvitations = CalendarItemCreateOrDeleteOperationType::SEND_ONLY_TO_ALL;
        $request->Items = new NonEmptyArrayOfAllItemsType();

        // Build the event to be added
        $event = new CalendarItemType();
        $event->RequiredAttendees = new NonEmptyArrayOfAttendeesType();
        $event->Start = $eventData->start->format('c');
        $event->End = $eventData->end->format('c');
        $event->Subject = $eventData->subject;


        // Set the event body.
        $event->Body = new BodyType();
        $event->Body->_ = 'This is the event body';
        $event->Body->BodyType = BodyTypeType::TEXT;

        // Iterate over the guests, adding each as an attendee to the request.
        foreach ($eventData->guests as $guest) {
            $attendee = new AttendeeType();
            $attendee->Mailbox = new EmailAddressType();
            $attendee->Mailbox->EmailAddress = $guest['email'];
            $attendee->Mailbox->Name = $guest['name'];
            $attendee->Mailbox->RoutingType = RoutingType::SMTP;
            $event->RequiredAttendees->Attendee[] = $attendee;
        }

        // Add the event to the request. You could add multiple events to create more
        // than one in a single request.
        $request->Items->CalendarItem[] = $event;
        $response = $this->client->CreateItem($request);
        // Iterate over the results, printing any error messages or event ids.
        $response_messages = $response->ResponseMessages->CreateItemResponseMessage;

        $successful_responses = [];

        foreach ($response_messages as $response_message) {
            // Make sure the request succeeded.
            if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {
                $code = $response_message->ResponseCode;
                $message = $response_message->MessageText;
                fwrite(STDERR, "Contact failed to create with \"$code: $message\"\n");
                continue;
            } else {
                $successful_responses[] = $response_message;
            }
        }

        return $successful_responses;
    }

    public function cancelEvent($event)
    {
        $request = new CreateItemType();
        $request->MessageDisposition = MessageDispositionType::SEND_AND_SAVE_COPY;
        $request->Items = new NonEmptyArrayOfAllItemsType();
        $cancellation = new CancelCalendarItemType();
        $cancellation->ReferenceItemId = new ItemIdType();
        $cancellation->ReferenceItemId->Id = $event['id'];
        $cancellation->ReferenceItemId->ChangeKey = $event['change_key'];
        $request->Items->CancelCalendarItem[] = $cancellation;
        $response = $this->client->CreateItem($request);
        // Iterate over the results, printing any error messages.
        $response_messages = $response->ResponseMessages->CreateItemResponseMessage;

        $successful_responses = [];

        foreach ($response_messages as $response_message) {
            // Make sure the request succeeded.
            if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {
                $code = $response_message->ResponseCode;
                $message = $response_message->MessageText;
                fwrite(
                    STDERR,
                    "Cancellation failed to create with \"$code: $message\"\n"
                );
                continue;
            } else {
                $successful_responses[] = $response_message;
            }
        }

        return $successful_responses;
    }

    public function updateEvents($event_updates)
    {
        // Build the request.
        $request = new UpdateItemType();
        $request->ConflictResolution = ConflictResolutionType::ALWAYS_OVERWRITE;
        $request->SendMeetingInvitationsOrCancellations = CalendarItemUpdateOperationType::SEND_TO_ALL_AND_SAVE_COPY;

        // Iterate over the updated to be applied, adding each to the request.
        // TODO: refactor this to set the supplied values dynamically for each event update
        foreach ($event_updates as $update) {
            // Build out item change request.
            $change = new ItemChangeType();
            $change->ItemId = new ItemIdType();
            $change->ItemId->Id = $update['id'];
            $change->Updates = new NonEmptyArrayOfItemChangeDescriptionsType();
            // Set the updated start time.
            $field = new SetItemFieldType();
            $field->FieldURI = new PathToUnindexedFieldType();
            $field->FieldURI->FieldURI = UnindexedFieldURIType::CALENDAR_START;
            $field->CalendarItem = new CalendarItemType();
            $field->CalendarItem->Start = $update['start']->format('c');
            $change->Updates->SetItemField[] = $field;
            // Set the updated end time.
            $field = new SetItemFieldType();
            $field->FieldURI = new PathToUnindexedFieldType();
            $field->FieldURI->FieldURI = UnindexedFieldURIType::CALENDAR_END;
            $field->CalendarItem = new CalendarItemType();
            $field->CalendarItem->End = $update['end']->format('c');
            $change->Updates->SetItemField[] = $field;
            $request->ItemChanges[] = $change;
        }

        $response = $this->client->UpdateItem($request);

        // Iterate over the results, printing any error messages or ids of events that
        // were updated.
        $response_messages = $response->ResponseMessages->UpdateItemResponseMessage;

        $successful_responses = [];

        foreach ($response_messages as $response_message) {
            // Make sure the request succeeded.
            if ($response_message->ResponseClass != ResponseClassType::SUCCESS) {
                $code = $response_message->ResponseCode;
                $message = $response_message->MessageText;
                fwrite(STDERR, "Failed to update event with \"$code: $message\"\n");
                continue;
            }

            $successful_responses[] = $response_message;
        }

        return $successful_responses;
    }
}