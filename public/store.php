<?php

class Store {

    private $data = [];

    public function __construct()
    {
        $this->data = $this->buildData();
    }

    private function buildData ()
    {
        return [
            'contacts' => [
                [
                    'id' => 1,
                    'remote_id' => 'AAMkADA3MGQwZDJkLWI1MzgtNGE4Ny1iMmViLTczNDgyY2I0ZjNiNQBGAAAAAACESTJyceUwTqjsXR+3RPmCBwB66EMkOaOGTJNI2LykEPHDAAAAAAEOAAB66EMkOaOGTJNI2LykEPHDAAAvgTNNAAA='
                ],
                [
                    'id' => 2,
                    'remote_id' => 'AAMkADA3MGQwZDJkLWI1MzgtNGE4Ny1iMmViLTczNDgyY2I0ZjNiNQBGAAAAAACESTJyceUwTqjsXR+3RPmCBwB66EMkOaOGTJNI2LykEPHDAAAAAAEOAAB66EMkOaOGTJNI2LykEPHDAAAvgTNMAAA='
                ],
                [
                    'id' => 3,
                    'remote_id' => 'AAMkADA3MGQwZDJkLWI1MzgtNGE4Ny1iMmViLTczNDgyY2I0ZjNiNQBGAAAAAACESTJyceUwTqjsXR+3RPmCBwB66EMkOaOGTJNI2LykEPHDAAAAAAEOAAB66EMkOaOGTJNI2LykEPHDAAAvgTNLAAA='
                ],
                [
                    'id' => 4,
                    'remote_id' => 'AAMkADA3MGQwZDJkLWI1MzgtNGE4Ny1iMmViLTczNDgyY2I0ZjNiNQBGAAAAAACESTJyceUwTqjsXR+3RPmCBwB66EMkOaOGTJNI2LykEPHDAAAAAAEOAAB66EMkOaOGTJNI2LykEPHDAAAvgTNKAAA='
                ],
                [
                    'id' => 5,
                    'remote_id' => 'AAMkADA3MGQwZDJkLWI1MzgtNGE4Ny1iMmViLTczNDgyY2I0ZjNiNQBGAAAAAACESTJyceUwTqjsXR+3RPmCBwB66EMkOaOGTJNI2LykEPHDAAAAAAEOAAB66EMkOaOGTJNI2LykEPHDAAAzo0SIAAA='
                ]
            ],
            'events' => [
                [
                    'id' => 1,
                    'change_key' => 'DwAAABYAAAB66EMkOaOGTJNI2LykEPHDAAAu6mtQ',
                    'remote_id' => 'AAMkADA3MGQwZDJkLWI1MzgtNGE4Ny1iMmViLTczNDgyY2I0ZjNiNQBGAAAAAACESTJyceUwTqjsXR+3RPmCBwB66EMkOaOGTJNI2LykEPHDAAAAAAENAAB66EMkOaOGTJNI2LykEPHDAAAtzrj2AAA='
                ],
                [
                    'id' => 2,
                    'change_key' => 'DwAAABYAAAB66EMkOaOGTJNI2LykEPHDAAArj/BX',
                    'remote_id' => 'AAMkADA3MGQwZDJkLWI1MzgtNGE4Ny1iMmViLTczNDgyY2I0ZjNiNQBGAAAAAACESTJyceUwTqjsXR+3RPmCBwB66EMkOaOGTJNI2LykEPHDAAAAAAENAAB66EMkOaOGTJNI2LykEPHDAAArj9qxAAA='
                ],
                [
                    'id' => 3,
                    'change_key' => 'DwAAABYAAAB66EMkOaOGTJNI2LykEPHDAAA1CSZ1',
                    'remote_id' => 'AAMkADA3MGQwZDJkLWI1MzgtNGE4Ny1iMmViLTczNDgyY2I0ZjNiNQBGAAAAAACESTJyceUwTqjsXR+3RPmCBwB66EMkOaOGTJNI2LykEPHDAAAAAAENAAB66EMkOaOGTJNI2LykEPHDAAA1CIk3AAA='
                ],
                [
                    'id' => 4,
                    'change_key' => 'DwAAABYAAAB66EMkOaOGTJNI2LykEPHDAAA1CSZ0',
                    'remote_id' => 'AAMkADA3MGQwZDJkLWI1MzgtNGE4Ny1iMmViLTczNDgyY2I0ZjNiNQBGAAAAAACESTJyceUwTqjsXR+3RPmCBwB66EMkOaOGTJNI2LykEPHDAAAAAAENAAB66EMkOaOGTJNI2LykEPHDAAA1CIk2AAA='
                ],
                [
                    'id' => 5,
                    'change_key' => 'DwAAABYAAAB66EMkOaOGTJNI2LykEPHDAAA1CSZz',
                    'remote_id' => 'AAMkADA3MGQwZDJkLWI1MzgtNGE4Ny1iMmViLTczNDgyY2I0ZjNiNQBGAAAAAACESTJyceUwTqjsXR+3RPmCBwB66EMkOaOGTJNI2LykEPHDAAAAAAENAAB66EMkOaOGTJNI2LykEPHDAAA1CIk1AAA='
                ]
            ]
        ];
    }

    public function find ($type, $id)
    {
        $data = $this->data[$type];

        $found = false;

        foreach($data as $d) {
            if ($found) {
                continue;
            }

            if ($d['id'] == $id) {
                $found = $d;
            }
        }

        return $found;
    }

    public function findAll($type)
    {
        return $this->data[$type];
    }
}