<?php

require 'store.php';

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

use \Libraries\Exchange\Contacts\Contacts;
use \Libraries\Exchange\Contacts\Contact;
use \Libraries\Exchange\Events\Events;
use \Libraries\Exchange\Events\Event;

$isDevMode = true;
$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/src"), $isDevMode);

$conn = array(
    'driver' => 'pdo_sqlite',
    'path' => __DIR__ . '/db.sqlite',
);

$entityManager = EntityManager::create($conn, $config);

class App {

    private $host = '';
    private $username = '';
    private $password = '';

    private $exchangeContacts = null;
    private $exchangeEvents = null;

    public function __construct()
    {
        $this->host = 'outlook.office365.com';
        // These would probably be somewhere else in a real app
        $this->username = '';
        $this->password = '';

        // these are the actual classes that wrap the php-ews stuff
        $this->exchangeContacts = new Contacts($this->host, $this->username, $this->password);
        $this->exchangeEvents = new Events($this->host, $this->username, $this->password);

        // this is the data fixture store
        $this->store = new Store();
    }

    /**
     * A function demonstrating how to use the exchangeContacts class to find all contacts
     *
     * @return \jamesiarmes\PhpEws\Type\ContactItemType[]
     */
    public function findContacts()
    {
        return $this->exchangeContacts->findContacts();
    }

    /**
     * A function demonstrating how to use the exchangeContacts class to describe a single contact
     *
     * @param $id
     * @return mixed
     */
    public function describeContact($id)
    {
        $contact = $this->store->find('contacts', $id);

        $contactDescription = $this->exchangeContacts->describeContacts([
            $contact['remote_id']
        ]);

        return $contactDescription[0];
    }

    /**
     * A function demonstrating how to use the exchangeContacts class to describe multiple contacts
     *
     * @return array
     */
    public function describeAllContacts()
    {
        $contacts = $this->store->findAll('contacts');
        $contactDescriptions = $this->exchangeContacts->describeContacts(array_map(function($item) {
            return $item['remote_id'];
        }, $contacts));

        return $contactDescriptions;
    }

    /**
     * A function demonstrating how to use the exchangeContacts class to add a contact.
     *
     * @return array
     */
    public function addContact()
    {
        $contact = new Contact([
            'givenName' => $this->getRandomGivenName(),
            'surname' => $this->getRandomSurname(),
            'email' => 'sgkljsgl@slgjsgl.com',
            'street' => '1521 sksjgg',
            'city' => 'Atlanta',
            'state' => 'GA',
            'postalCode' => '30303',
            'countryOrRegion' => 'US',
            'title' => 'Mr.'
        ]);

        return $this->exchangeContacts->addContact($contact);
    }

    /**
     * A function demonstrating how to use the exchangeContacts class to update a contact
     *
     * @param bool $id
     * @return array
     */
    public function updateContacts($id = false)
    {
        $contact = $this->store->find('contacts', $id);

        return $this->exchangeContacts->updateContacts([[
            'id' => $contact['remote_id'],
            'givenName' => $this->getRandomGivenName(),
            'surname' => $this->getRandomSurname()
        ]]);
    }

    /**
     * A function demonstrating how to use the exchangeEvents class to find events
     *
     * @return \jamesiarmes\PhpEws\Type\CalendarItemType[]
     */
    public function findEvents()
    {
        return $this->exchangeEvents->findEvents([
            'start' => new DateTime('January 1 00:00:00'),
            'end' => new DateTime('December 31 23:59:59'),
            'timezone' => 'Eastern Standard Time'
        ]);
    }

    /**
     * A function demonstrating how to use the exchangeEvents class to describe a single event
     *
     * @param $id
     * @return mixed
     */
    public function describeEvent($id)
    {
        $event = $this->store->find('events', $id);

        $eventDescription = $this->exchangeEvents->describeEvents([
            $event['remote_id']
        ]);

        return $eventDescription[0];
    }

    /**
     * A function demonstrating how to use the exchangeEvents class to describe all events
     *
     * @return array
     */
    public function describeAllEvents()
    {
        $events = $this->store->findAll('events');
        $eventDescriptions = $this->exchangeEvents->describeEvents(array_map(function($item) {
            return $item['remote_id'];
        }, $events));

        return $eventDescriptions;
    }

    /**
     * A function demonstrating how to use the exchangeEvents class to add an event
     *
     * @return array
     */
    public function addEvent()
    {
        $event = new Event([
            'start' => new DateTime('tomorrow 4:00pm'),
            'end' => new DateTime('tomorrow 5:00pm'),
            'subject' => $this->getRandomEventSubject(),
            'guests' => [
                [
                    'name' => $this->getRandomGivenName() . ' ' . $this->getRandomSurname(),
                    'email' => $this->getRandomEmailAddress()
                ]
            ],
            'body' => 'This is the body of the event'
        ]);

        return $this->exchangeEvents->addEvent($event);
    }

    /**
     * A function demonstrating how to use the exchangeEvents class to cancel an event
     *
     * @param $id
     * @return array
     */
    public function cancelEvent($id)
    {
        $event = $this->store->find('events', $id);

        return $this->exchangeEvents->cancelEvent($event);
    }

    /**
     * A function demonstrating how to use the exchangeEvents class to update events
     *
     * @param $id
     * @return array
     */
    public function updateEvents($id)
    {
        $event = $this->store->find('events', $id);

        return $this->exchangeEvents->updateEvents([[
            'id' => $event['remote_id'],
            'start' => new DateTime('tomorrow 9:00pm'),
            'end' => new DateTime('tomorrow 9:30pm')
        ]]);
    }

    private function getRandomGivenName()
    {
        $names = array(
            'Christopher',
            'Ryan',
            'Ethan',
            'John',
            'Zoey',
            'Sarah',
            'Michelle',
            'Samantha',
        );

        return $names[mt_rand(0, sizeof($names) - 1)];
    }

    private function getRandomSurname()
    {
        $surnames = array(
            'Walker',
            'Thompson',
            'Anderson',
            'Johnson',
            'Tremblay',
            'Peltier',
            'Cunningham',
            'Simpson',
            'Mercado',
            'Sellers'
        );

        return $surnames[mt_rand(0, sizeof($surnames) - 1)];
    }

    private function getRandomEventSubject()
    {
        $subjects = array(
            'Standup',
            'Conference Call',
            'Lunch',
            'Phone Call',
            'Meeting'
        );

        return $subjects[mt_rand(0, sizeof($subjects) - 1)];
    }

    private function getRandomEmailAddress()
    {
        $a = '';

        $tlds = array("com", "net", "gov", "org", "edu", "biz", "info");
        $char = "0123456789abcdefghijklmnopqrstuvwxyz";

        $ulen = mt_rand(5, 10);
        $dlen = mt_rand(7, 17);

        for ($i = 1; $i <= $ulen; $i++) {
            $a .= substr($char, mt_rand(0, strlen($char)), 1);
        }

        $a .= "@";

        for ($i = 1; $i <= $dlen; $i++) {
            $a .= substr($char, mt_rand(0, strlen($char)), 1);
        }

        $a .= ".";

        $a .= $tlds[mt_rand(0, (sizeof($tlds)-1))];

        return $a;
    }
}