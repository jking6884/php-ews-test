<?php
require '../vendor/autoload.php';
require 'app.php';

/**
 * A simple router that points a url to a particular function in the app.php file.
 *
 * In the browser, after firing up the docker container, go to: localhost:8080/ followed by the segment in each case below
 *
 * E.G. localhost:8080/contacts/add
 */

$app = new App();

$els = explode("/", $_SERVER['REQUEST_URI']);
$url = '/' . $els[1]  . '/' . $els[2];

$id = false;

if (count($els) > 3) {
    $id = $els[3];
}

switch ($url) {
    case '/contacts/add':
        echo "<pre>";
        die(print_r($app->addContact()));
        break;

    case '/contacts/find':
        echo "<pre>";
        die(print_r($app->findContacts()));
        break;

    case '/contacts/describe':
        echo "<pre>";
        if ($id) {
            die(print_r($app->describeContact($id)));
        } else {
            die(print_r($app->describeAllContacts()));
        }
        break;

    case '/contacts/update':
        echo "<pre>";
        die(print_r($app->updateContacts($id)));
        break;

    case '/events/add':
        echo "<pre>";
        die(print_r($app->addEvent()));
        break;

    case '/events/cancel':
        echo "<pre>";
        if ($id) {
            die(print_r($app->cancelEvent($id)));
        } else {
            die("You must provide an event id");
        }
        break;

    case '/events/find':
        echo "<pre>";
        die(print_r($app->findEvents()));
        break;

    case '/events/describe':
        echo "<pre>";
        if ($id) {
            die(print_r($app->describeEvent($id)));
        } else {
            die(print_r($app->describeAllEvents($id)));
        }
        break;

    case '/events/update':
        echo "<pre>";
        die(print_r($app->updateEvents($id)));
        break;
}